<?php

/**
 * Could also look at parsing shapefiles manually: http://en.wikipedia.org/wiki/Shapefile#Shapefile_shape_format_.28.shp.29
 * Could also implement Iterator: http://php.net/manual/en/class.iterator.php
 */
class FarmapShapefileUncompressed {
	var $extracted_path = '';
  var $uri = '';

  function __construct($uri) {
    $this->uri = $uri;
	  $basename = basename($this->uri, '.shp');
	  $full_path = substr(drupal_realpath($this->uri), 0, count(drupal_realpath($this->uri)) - 5);
//dpm($full_path);
    $this->extracted_path = drupal_realpath('temporary://farmap_shapefile_'. $basename);
//dpm($this->extracted_path);
if(!is_dir($this->extracted_path)){
//drupal_set_message('mkdir');
$mkdir = mkdir($this->extracted_path);
}

    // Copy shp to temporary
	  $copy = copy(drupal_realpath($uri), $this->extracted_path.'/'.$basename.'.shp');
	  // Copy other files to temporary
	  $shape_extensions = array('shx', 'dbf', 'prj', 'sbn', 'sbx', 'fbn', 'fbx', 'ain', 'aih', 'ixs', 'mxs', 'atx', 'xml', 'cpg', 'qix');
	  foreach ($shape_extensions as $extension) {
		  if (file_exists($full_path.'.'.$extension)) {
//drupal_set_message('copy');
			  $copy = copy($full_path.'.'.$extension, $this->extracted_path.'/'.$basename.'.'.$extension);
		  }
	  }
  }

  function __destruct() {
    // Delete extracted folder.
    file_unmanaged_delete_recursive($this->extracted_path);
  }

  function process() {
    $result = array();
//dpm($this);
	  // Look for shapefiles.
    $shapefiles = file_scan_directory($this->extracted_path, '/^.*\.(shp)$/');
//dpm($shapefiles);

    foreach ($shapefiles as $shapefile) {
      if ($ogr2ogr_shapefile = ogr2ogr_open($shapefile->uri)) {
	      //dpm($ogr2ogr_shapefile);
	      //dpm($ogr2ogr_shapefile->getWkt('EPSG:3857'));
        if ($spatial_features = $ogr2ogr_shapefile->getWkt('EPSG:3857')) {
          $result = array_merge($result, $spatial_features);
        }
      }
    }

    return $result;
  }

}
