<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 10/8/15
 * Time: 5:13 PM
 */

require_once(drupal_get_path('module', 'geoserver').'/geoserver.layer.inc');

function farmap_geoserver_layer_type_layer_save(&$object) {
	$update = ($object->export_type & EXPORT_IN_DATABASE) ? array('name') : array();
	$object->export_type = EXPORT_IN_DATABASE;
	$result = drupal_write_record('geoserver_layers', $object, $update);
	if ($result) {
		$layer = geoserver_get_layer_object($object);
		return $layer->save();
	}
	else {
		return FALSE;
	}
}

class farmap_geoserver_layer_type {
public $openlayers_array = array(
'wfs' => 1,
'wfs_data' => array(
'strategy' => 'bbox',
'projection' => array(
'EPSG:3857' => 'EPSG:3857',
),
'isBaseLayer' => 0,
),
'gwc' => 0,
'gwc_data' => array(
'strategy' => 'bbox',
'isBaseLayer' => 0,
),
'wms' => 0,
'wms_data' => array(
'strategy' => 'bbox',
'isBaseLayer' => 0,
),
);
public $options_array = array(
'srid' => '4326',
'sql_name' => '',
'sql' => '',
'bounding_box' => array(
'minx' => '-180',
'miny' => '-90',
'maxx' => '180',
'maxy' => '90',
),
);
public $data_array = array(
'type' => 'farmap_layer_type_postgis_field',
'style' => '',
'source' => '',
'attribution' => '',
'openlayers' => '',
'options' => '',
);
	/**
	 * Constructor.
	 */
	function __construct($name, $layer_name, $bbox) {
		$this->data_array['openlayers'] = $this->openlayers_array;
		$this->data_array['options'] = $this->options_array;
        $this->data_array['options']['bounding_box']['minx'] = $bbox['minx'];
        $this->data_array['options']['bounding_box']['miny'] = $bbox['miny'];
        $this->data_array['options']['bounding_box']['maxx'] = $bbox['maxx'];
        $this->data_array['options']['bounding_box']['maxy'] = $bbox['maxy'];
		$this->name = $name;
		//$this->title = $name;
		$this->title = $layer_name;
		$this->description = $name." layer";
		$this->data = $this->data_array;
		$this->export_type = '';
		$this->type = 'Local';
		$this->datastore = variable_get('geoserver_postgis_datastore', '');
	}

	/**
	 * Load field configuration.
	 */
	function load_source($source) {

		$prefix       = Database::getConnection()->tablePrefix();
		$source_array = explode('.', $source);
		$entity_type  = $source_array[0];
		$bundle_name  = $source_array[1];
		$field_name   = $source_array[2];
		$nid          = $source_array[3];
$delta = $source_array[4];
		$sql_name     = $prefix . 'geoserver_' . $this->name . '_view';
		$sql          = $this->get_sql($entity_type, $bundle_name, $field_name, $nid, $sql_name, $delta);
		$field        = field_info_field($field_name);

		if ($field['settings']['srid'] == '4326') {
			//$bbox = array('-180', '-90', '180', '90');
            $bbox = array($this->data_array['options']['bounding_box']['minx'],$this->data_array['options']['bounding_box']['miny'],$this->data_array['options']['bounding_box']['maxx'],$this->data_array['options']['bounding_box']['maxy']);
		}
		elseif ($field['settings']['srid'] == '900913') {
			$bbox = array('-20037508', '-20037508', '20037508', '20037508');
		}
		else {
			// Try to calculate maximum bounding box for defined projection.
			try {
				$extent = db_query("select st_extent(st_transform(st_setsrid(st_geomfromtext('polygon((-180 -90,-180 90, 180 90, -180 -90))'), 4326), :srid::int))",
					array(':srid' => $field['settings']['srid']))->fetchField();
				$bbox = preg_split('/[\, ]/', drupal_substr($extent, 4, -1));
			} catch (PDOException $exc) {
				$bbox = array('', '', '', '');
			}
		}

		$this->data['source'] = $source;
		$this->data['options']['srid'] = $field['settings']['srid'];
		$this->data['options']['sql_name'] = $sql_name;
		$this->data['options']['sql'] = $sql;
		$this->data['options']['bounding_box']['minx'] = $bbox[0];
		$this->data['options']['bounding_box']['miny'] = $bbox[1];
		$this->data['options']['bounding_box']['maxx'] = $bbox[2];
		$this->data['options']['bounding_box']['maxy'] = $bbox[3];
	}

        function load_style($style) {
               $this->data['style'] = $style;
        }

	/**
	 * Get layer SQL definition.
	 */
	protected function get_sql($entity_type, $bundle_name, $field_name, $nid, $name, $delta) {
		$prefix = Database::getConnection()->tablePrefix();
		$rule_name = str_replace('.', '_', $name);
		$sql = array(
			'select' => array("DROP VIEW IF EXISTS $name;
CREATE VIEW $name AS SELECT $entity_type.nid AS id, $entity_type.nid, $entity_type.title AS name, field_data_$field_name.delta, field_data_$field_name.{$field_name}_geometry"),
			'from' => array("FROM $prefix$entity_type LEFT JOIN {$prefix}field_data_$field_name ON node.nid = field_data_$field_name.entity_id"),
			'where' => array("WHERE $entity_type.nid = '$nid' and $entity_type.status = 1 and field_data_$field_name.delta = '$delta' and field_data_$field_name.deleted = 0"),
			'rules' => array(
				"CREATE OR REPLACE RULE ${rule_name}_update AS ON UPDATE TO $name DO INSTEAD
  UPDATE {$prefix}field_data_$field_name SET ${field_name}_geometry = NEW.${field_name}_geometry WHERE entity_id = NEW.id;",
				"CREATE OR REPLACE RULE ${rule_name}_insert AS ON INSERT TO ${name} DO INSTEAD (
    INSERT INTO {$prefix}field_data_$field_name VALUES ('node', '$bundle_name', 0, nextval('{$prefix}node_nid_seq'), currval('{$prefix}node_nid_seq'), 'und', 0, NEW.${field_name}_geometry);
    INSERT INTO {$prefix}node VALUES (currval('{$prefix}node_nid_seq'), currval('{$prefix}node_nid_seq'), '$bundle_name', 'und', '', 0, 1, extract(epoch from now())::int, extract(epoch from now())::int, 0, 0, 0, 0, 0);
    INSERT INTO {$prefix}node_revision VALUES (currval('{$prefix}node_nid_seq'), currval('{$prefix}node_nid_seq'), 0, '', '', extract(epoch from now())::int, 1, 0, 0, 0);
  );",
				"CREATE OR REPLACE RULE ${rule_name}_delete AS ON DELETE TO ${name} DO INSTEAD
  DELETE FROM {$prefix}field_data_$field_name  WHERE entity_id = OLD.id and delta = OLD.delta;",
			),
		);

		// Join additional fields with a 1:1 mapping to this bundle.
		$fields = field_info_instances($entity_type, $bundle_name);
		foreach ($fields as $name => $field) {
			$field = field_info_field($name);
			if ($name != $field_name && $field['cardinality'] === '1') {
				foreach ($field['storage']['details']['sql']['FIELD_LOAD_CURRENT'] as $table => $table_fields) {
					foreach ($table_fields as $table_field) {
						if ($table_field === 'body_value') {
							$table_field .= ' AS description';
						}
						$sql['select'][] = $table_field;
					}
					$sql['from'][] = "LEFT JOIN $prefix$table ON $entity_type.nid = $table.entity_id";
				}
			}
		}
		return implode(', ', $sql['select']) . "\n" . implode("\n", $sql['from']) . "\n" . implode("\n", $sql['where']) . ";\n" . implode("\n", $sql['rules']);
	}

}
