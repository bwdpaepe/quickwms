/**
 * Created by bart on 9/15/15.
 */
(function ($) {
	$(document).ready(function() {
		$('#edit-field-projection').hide();
		$('#edit-field-farmap-file').change(function (evt) {
			/*get the zip file
			 * extract it
			 * check for prj file
			 * */
			var files = evt.target.files;
			$.each(files, function (i, file) {
				if (file.type === "application/zip") {
					// https://thiscouldbebetter.wordpress.com/2013/08/06/reading-zip-files-in-javascript-using-jszip/
					var zipFileToLoad = file;
					var fileReader = new FileReader();
					fileReader.onload = function(fileLoadedEvent)	{
						var zipFileLoaded = new JSZip(fileLoadedEvent.target.result);
						for (var nameOfFileContainedInZipFile in zipFileLoaded.files) {
							var ext =  nameOfFileContainedInZipFile.split('.').pop();
							if(ext === 'prj') {
								var prj_check = true;
								break;
							}
							else {
								var prj_check = false;
							}
						}
						if (prj_check) {
							alert('The zip file you provided contains projection information.')
						}
						else{
							$('#edit-field-projection').show();
						}
					};
					fileReader.readAsArrayBuffer(zipFileToLoad);
				}
				else {
					alert('The file MIME type is "' + file.type + '"');
				}
			});
		});
	});
})(jQuery);
