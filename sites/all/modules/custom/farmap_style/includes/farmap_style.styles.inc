<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 3/1/16
 * Time: 8:41 AM
 */

/**
 * @file
 * This file contains styles implementations.
 *
 * @ingroup openlayers
 */

/**
 * Internal callback
 * Helper function to return default styles.
 *
 * @return mixed
 */
function _farmap_style_openlayers_styles() {
	$styles = array();

	// Default style
	$style = new stdClass();
	$style->api_version = 1;
	$style->name = 'rain_1';
	$style->title = t('Rainbow style 1');
	$style->description = t('Rainbow style 1.');
	$style->data = array(
		'fillColor' => '#ee4035',
		'strokeColor' => '#ee4035',
'strokeWidth' => '4',
		'fillOpacity' => '0.7',
		'strokeOpacity' => '0.7',
	);
	$styles[$style->name] = $style;

	// Default style
	$style = new stdClass();
	$style->api_version = 1;
	$style->name = 'rain_2';
	$style->title = t('Rainbow style 2');
	$style->description = t('Rainbow style 2.');
	$style->data = array(
		'fillColor' => '#f37736',
		'strokeColor' => '#f37736',
'strokeWidth' => '4',
		'fillOpacity' => '0.7',
		'strokeOpacity' => '0.7',
	);
	$styles[$style->name] = $style;

	// Default style
	$style = new stdClass();
	$style->api_version = 1;
	$style->name = 'rain_3';
	$style->title = t('Rainbow style 3');
	$style->description = t('Rainbow style 3.');
	$style->data = array(
		'fillColor' => '#fdf498',
		'strokeColor' => '#fdf498',
'strokeWidth' => '4',
		'fillOpacity' => '0.7',
		'strokeOpacity' => '0.7',
	);
	$styles[$style->name] = $style;

	// Default style
	$style = new stdClass();
	$style->api_version = 1;
	$style->name = 'rain_4';
	$style->title = t('Rainbow style 4');
	$style->description = t('Rainbow style 4.');
	$style->data = array(
		'fillColor' => '#7bc043',
		'strokeColor' => '#7bc043',
'strokeWidth' => '4',
		'fillOpacity' => '0.7',
		'strokeOpacity' => '0.7',
	);
	$styles[$style->name] = $style;

	// Default style
	$style = new stdClass();
	$style->api_version = 1;
	$style->name = 'rain_5';
	$style->title = t('Rainbow style 5');
	$style->description = t('Rainbow style 5.');
	$style->data = array(
		'fillColor' => '#0392cf',
		'strokeColor' => '#0392cf',
'strokeWidth' => '4',
		'fillOpacity' => '0.7',
		'strokeOpacity' => '0.7',
	);
	$styles[$style->name] = $style;

	// Default style
	$style = new stdClass();
	$style->api_version = 1;
	$style->name = 'rain_6';
	$style->title = t('Rainbow style 6');
	$style->description = t('Rainbow style 6.');
	$style->data = array(
		'fillColor' => '#cccccc',
		'strokeColor' => '#cccccc',
'strokeWidth' => '4',
		'fillOpacity' => '0.7',
		'strokeOpacity' => '0.7',
	);
	$styles[$style->name] = $style;


	return $styles;
}
