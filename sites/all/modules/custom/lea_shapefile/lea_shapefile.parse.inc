<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 12/15/17
 * Time: 3:19 PM
 */

// Internal classes
require_once __DIR__.'/ShapeFileLoader.class.php';
// Import classes
use \lea_shapefile\ShapeFileLoader;

// External classes
require_once libraries_get_path('php_shapefile') . '/src/ShapeFileAutoloader.php';
// Import classes
use \ShapeFile\ShapeFile;
use \ShapeFile\ShapeFileException;
\ShapeFile\ShapeFileAutoloader::register();

function test_parse_shapefile($fileName=NULL)
{
        $shapefile_output = '';
        if(!is_null($fileName)) {
            $shapeFileLoader_configuration = array(
                'input_dir' => __DIR__ . "/resources/input/",
                'storage_dir' => __DIR__ . "/resources/storage/",
                'knooppunten_filename' => 'DELTA-Knooppunten',
                'trajecten_filename' => 'DELTA-Trajecten',
                'filename' => $fileName,
            );
            $ShapeFileLoader = new ShapeFileLoader($shapeFileLoader_configuration);

            $shapeFile = $ShapeFileLoader->getShapeFile();
            $shapefile_array = shapefile_class($shapeFile);




        }
        else{
            $shapeFileLoader_configuration = array(
                'input_dir' => __DIR__ . "/resources/input/",
                'storage_dir' => __DIR__ . "/resources/storage/",
                'knooppunten_filename' => 'DELTA-Knooppunten',
                'trajecten_filename' => 'DELTA-Trajecten',
            );
            $ShapeFileLoader = new ShapeFileLoader($shapeFileLoader_configuration);

            $shapefile_array = $ShapeFileLoader->getShapeFiles();

            $shapefile_output = '';
            if(count($shapefile_array) >= 1) {
                foreach ($shapefile_array as $shapeFile) {
                    $shapefile_array = shapefile_class($shapeFile);
                }
            }
        }

        if(is_null($shapefile_array)) {
            return $shapefile_output;
        }
        else {
            return $shapefile_array;
        }
}

function shapefile_class($shapeFileString) {
    $shapefile_array = array();
    $shapefile_array['geometry'] = array();
    $shapefile_array['attributes'] = array();
    try {
        // Open shapefile
        $ShapeFile = new ShapeFile($shapeFileString);

        // Sets default return format
        $ShapeFile->setDefaultGeometryFormat($ShapeFile::GEOMETRY_GEOJSON_GEOMETRY);

        // Read all the records
        while ($record = $ShapeFile->getRecord(ShapeFile::GEOMETRY_WKT)) {
            if ($record['dbf']['_deleted']) continue;
            // Geometry
            $shapefile_geometry = $record['shp'];
            $shapefile_array['geometry'][] = $shapefile_geometry;
            // DBF Data
            $shapefile_attributes = $record['dbf'];
            $shapefile_array['attributes'][] = $shapefile_attributes;


        }

    } catch (ShapeFileException $e) {
        // Print detailed error information
        exit('Error ' . $e->getCode() . ' (' . $e->getErrorType() . '): ' . $e->getMessage());
    }
    return $shapefile_array;
}