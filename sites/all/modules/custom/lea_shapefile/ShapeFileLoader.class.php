<?php
namespace lea_shapefile;

class ShapeFileLoader
{
    private $inputDir;

    private $storageDir;

    private $knooppuntFileName;

    private $trajectFileName;

    private $fileName;

    /**
     * @var ShapeFileLoader
     */
    private static $instance = null;

    /**
     * @return ShapeFileLoader
     */
    public static function getInstance()
    {
        if(self::$instance === null)
            self::$instance = new ShapeFileLoader();

        return self::$instance;
    }



    public function __construct($import_settings = array())
    {
        $current_YYYY_MM_DD = date("Y-m-d");
        if(isset($import_settings['filename'])) {
            $this->fileName = $import_settings['filename'];
        }
        else {
            $this->inputDir = $import_settings['input_dir'] . 'delta_' . $current_YYYY_MM_DD;
            $this->storageDir = $import_settings['storage_dir'];
            $this->knooppuntFileName = $import_settings['knooppunten_filename'];
            $this->trajectFileName = $import_settings['trajecten_filename'];
        }
    }

    /**
     * @return AbstractShip[]
     */
    public function getShapeFiles()
    {
        $shapeFiles = array();
        $allFiles = scandir($this->inputDir);

        foreach ($allFiles as $candidateFile) {
            $file_parts = pathinfo($candidateFile);
            if ($file_parts['extension'] == 'shp') {
                $shapeFiles[] = $this->inputDir.'/'.$candidateFile;
            }
        }

        return $shapeFiles;
    }

    public function getShapeFile()
    {

        $file_parts = pathinfo($this->fileName);
        if ($file_parts['extension'] == 'shp') {
            $shapeFile = $this->inputDir.'/'.$this->fileName;
        }


        return $shapeFile;
    }
}

