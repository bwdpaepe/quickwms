/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * Attribution Behavior
 */
Drupal.openlayers.addBehavior('farmap_behavior_mouseover', function (data, options) {
	var map = data.openlayers;
	function farmapVisibility(evt) {
		var patt = /fl.*/i;
		var mLayers = map.getLayersByName(patt);
		//var mLayers = map.layers;

		for(var a = 0; a < mLayers.length; a++ ){
			mLayers[a].setVisibility(true);
		};
		map.events.unregister('mouseover', map, farmapVisibility);
		/*var layers = map.layers;
		alert(layers[0].name);
		jQuery(layers).each(function(index, layer) {
			alert(layer[0].name);
			//layer, setVisibility(true);
		});*/
	};
	map.events.register('mouseover', map, farmapVisibility);
});
