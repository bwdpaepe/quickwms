<?php
/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * Fullscreen Behavior
 */
class farmap_behavior_mouseover extends openlayers_behavior {
  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
      'activated' => FALSE
    );
  }

  /**
   * Provide form for configurations per map.
   */
  function options_form($defaults = array()) {
    return array(
      'activated' => array(
        '#title' => t('Initially activated'),
        '#type' => 'checkbox',
        '#description' => t('Mouseover event activated by default.'),
        '#default_value' => isset($defaults['activated']) ?  $defaults['activated'] : FALSE
      )
    );
  }

  function js_dependency() {
    return array(
      'OpenLayers.Control.Button'
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'farmap_mouseover') .
      '/plugins/farmap_behavior_mouseover.js');
    return $this->options;
  }
}

