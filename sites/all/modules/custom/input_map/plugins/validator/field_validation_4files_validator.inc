<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 12/29/15
 * Time: 8:36 AM
 */

/**
 * @file
 * Field validation email validator.
 *
 */
$plugin = array(
	'label' => t('4files'),
	'description' => t("Verifies that user-entered data are good files."),
	'handler' => array(
		'class' => 'field_validation_4files_validator',
	),
);

class field_validation_4files_validator extends field_validation_validator {

	/**
	 * Validate field.
	 */
	public function validate() {
		$file_name = array();
		$extension = array();
		$match = '';
		$shp = FALSE;
		$dbf = FALSE;
		$shx = FALSE;
		$kml = FALSE;
		$num = 0;
		if (count($this->items) > 13) {
			drupal_goto('file_validate_message');
		}
		else {
			foreach ($this->items as $delta => $item) {
				if ($item['fid'] != 0) {
					$file = file_load($item['fid']);
					$file_name[] = array_shift(explode(".", $file->filename));
					$extension[] = end(explode(".", $file->filename));
					if ($delta == 0) {
						$match = $file_name[0];
					}
					$num++;
				}
			}
			for ($i = 0; $i < $num; $i++) {
				$result = preg_match('/' . $match . '/', $file_name[$i]);
				if ($result == 0) {
					$this->set_error();
					break;
				}
				if ($extension[$i] == 'shp'){
					$shp = TRUE;
				}
				if ($extension[$i] == 'dbf'){
					$dbf = TRUE;
				}
				if ($extension[$i] == 'shx'){
					$shx = TRUE;
				}
				if ($extension[$i] == 'kml'){
					$kml = TRUE;
				}
			}
			if ($shp && $kml){ // both shp and kml are uploaded
				drupal_goto('file_validate_message');
			}
			if (!$shp && !$kml && num > 1) { // neither shp or kml are uploaded
				drupal_goto('file_validate_message');
			}
			if ($kml && $num > 1) {
				drupal_goto('file_validate_message');
			}
			if($shp){
				if(!$dbf || !$shx){
//$this->set_error();
drupal_goto('file_validate_message');
				}
			}
		}
	}
}
