<?php if($items):?>
<div class="maxWidthContent homePage">
	<div class="row spacing headerHomeHeight">
         <?php 
            foreach ($items as $key=>$fieldCollectionItem):
            $fieldCollectionItemEntity = array_values($fieldCollectionItem['entity']['field_collection_item']);
            
            $fieldCollectionItemEntity = array_shift($fieldCollectionItemEntity);
            $fieldCollectionItemEntity = $fieldCollectionItemEntity['#entity'];
            /* @var $fieldCollectionItemEntityWrapper EntityDrupalWrapper */
            $fieldCollectionItemEntityWrapper = entity_metadata_wrapper('field_collection_item', $fieldCollectionItemEntity);
            ?>
		<div class="col-xs-12 col-md-4 text-center">
			<div class="miniblock block0">

				<div class="region region-homepage-block0">
					<div id="block-block-21"
						class="block block-block contextual-links-region">

						<h1><?php echo htmlentities($fieldCollectionItemEntityWrapper->field_title->value())?></h1>

						<div class="content">
							<p><?php echo htmlentities($fieldCollectionItemEntityWrapper->field_body->value());?></p>
						</div>
					</div>
				</div>
                <?php 
                /*$image = $fieldCollectionItemEntityWrapper->field_image->value();
                $image = image_style_url('visionary_style',$image['uri']);*/
                ?>
				<!-- <img class="img-circle myPicture"
					src="<?php //echo $image;?>"> <br> -->
				<!-- <br/> -->
				<?php 
                $actionlink = $fieldCollectionItemEntityWrapper->field_action_link->value();
                ?>
					<button class="btn btn-lg btn-default btnPerso"
						onclick="javascript:document.location.href='<?php echo htmlentities($actionlink['url'], ENT_QUOTES);?>'"><?php echo $actionlink['title']?></button>

			</div>
		</div>
        <?php endforeach;?>
	</div>
</div>
<?php endif;?>