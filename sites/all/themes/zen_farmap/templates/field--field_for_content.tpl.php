<?php if($items):?>
<div class="bgWhite">
	<div class="maxWidthContent homePage">
		<div class="row spacing ">
            <?php 
            $index = 0;
            foreach ($items as $key=>$fieldCollectionItem):
            ++$index;
            $fieldCollectionItemEntity = array_values($fieldCollectionItem['entity']['field_collection_item']);
            
            $fieldCollectionItemEntity = array_shift($fieldCollectionItemEntity);
            $fieldCollectionItemEntity = $fieldCollectionItemEntity['#entity'];
            /* @var $fieldCollectionItemEntityWrapper EntityDrupalWrapper */
            $fieldCollectionItemEntityWrapper = entity_metadata_wrapper('field_collection_item', $fieldCollectionItemEntity);
            ?>
			<div class="col-xs-12 col-md-4 <?php //echo $index != count($items)?'blockBorderRight':''?> marginLine">
				<div class="miniblock block1">

					<div class="region region-homepage-block1">
						<div id="block-block-5"
							class="block block-block contextual-links-region">
                            <?php
                            $image = $fieldCollectionItemEntityWrapper->field_image->value();
                            if(!empty($image)) {
                                $image = image_style_url('for_style', $image['uri']);
                                $showImage = true;
                            }
                            else{
                                $showImage = false;
                            }
                            ?>
                            <?php if($showImage):?>
                            <img class="img-rectangle forPicture"
                                 src="<?php echo $image;?>"> <br>
                            <?php endif;?>
							<h2><?php echo htmlentities($fieldCollectionItemEntityWrapper->field_title->value())?></h2>
							<div class="content">
								<p><?php print $fieldCollectionItemEntityWrapper->field_body->value()['value'];?></p>
							</div>
						</div>
					</div>

				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
</div>
<?php endif;?>