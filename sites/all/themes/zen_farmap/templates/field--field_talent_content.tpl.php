
<?php foreach ($items as $key=>$fieldCollectionItem):

$fieldCollectionItemEntity = array_values($fieldCollectionItem['entity']['field_collection_item']);

$fieldCollectionItemEntity = array_shift($fieldCollectionItemEntity);
$fieldCollectionItemEntity = $fieldCollectionItemEntity['#entity'];
/* @var $fieldCollectionItemEntityWrapper EntityDrupalWrapper */
$fieldCollectionItemEntityWrapper = entity_metadata_wrapper('field_collection_item', $fieldCollectionItemEntity);

?>
<div class="bgWhite">
	<div class="maxWidthContent homePage homePageColor1">
		<div class="row spacing">
			<div class="col-md-offset-1  col-xs-12 col-md-10 text-center">

				<div class="region region-homepage-block4">
					<div id="block-block-8"
						class="block block-block contextual-links-region">

						<h2><?php echo htmlentities($fieldCollectionItemEntityWrapper->field_title->value())?></h2>
						<div class="content">
							
							<?php echo $fieldCollectionItemEntityWrapper->field_body->value();?>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<?php endforeach;?>